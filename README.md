# Laboratoire #2

## Mon premier projet en C

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum dolor sit amet.

## Les fichiers pertinents pour le labo

Les solutions #3 du laboratoire 2 sont décrites par les fichiers suivants:

```
├── array.c
├── hello.c
├── Makefile
└── README.md
```

## Compilation

On utilise `make` comme vu dans le laboratoire. Aussi, il est possible de
compiler les programmes en mode « debug ». Pour ce faire, vous donnez une valeur
non-vide à la variable d'environnement `DEBUG` lors de l'appel de `make` comme
suit:

```sh
$ make DEBUG=oui
gcc -g -c -Wall hello.c
gcc -o hello hello.o
gcc -g -c -Wall array.c
gcc -o array array.o
```

Remarquez l'option `-g`. Afin de voir comment ça marche, regardez la variable
`CFLAGS` du fichier `Makefile`.

## Fichiers additionnels

Le fichier `print_array.h` contient du code que j'ai écrit afin de rendre
l'affichage plus agréable à l'exécution du programme. Si cela vous intéresse, je
vous conseille de lire ce fichier (très court) afin de comprendre ce code.

## Auteurs

Simon Désaulniers (desaulniers.simon@courrier.uqam.ca)

