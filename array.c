//----------------------------------------------------------
// Fichier: array.c             Auteur(s): Simon Désaulniers
// Date: 2018-01-22
//----------------------------------------------------------
// Laboratoire #2 du cours INF3135 à la session d'hiver
// 2018 à l'Université du Québec à Montréal.
//----------------------------------------------------------
#include <stdio.h>
#include <stdbool.h>

// petit fichier supplémentaire avec du code ne concernant pas le labo. Il
// s'agit de code qui permet de convertir un tableau en chaîne de caractères.
#include "print_array.h"

/***************************
*  Début du code du labo  *
***************************/

#define N_ENTIERS 4
#define N_ENTIERS_PAS_TRIE 6

int MY_INTEGERS[]          = { 1, 2, 3, 4 };
int MY_OTHER_INTEGERS[]    = { 3, 3, 16, 7 };
int MY_UNSORTED_INTEGERS[] = { 4, 1, 10, 80, 3, 30 };

/**************************************
 *  SOMME DE NOMBRES DANS UN TABLEAU  *
 **************************************/
int sum(int* integers, int len) {
    int sum_ = 0;

    for (size_t i = 0; i < len; ++i) {
        sum_ += integers[i];
    }

    return sum_;
}

/**********************************
 *  VÉRIFICATION SI TABLEAU TRIÉ  *
 **********************************/
bool is_sorted(int* integers, int len) {
    for (size_t i = 1; i < len; ++i) {
        if (integers[i-1] > integers[i])
            return false;
    }
    return true;
}

/******************************
 *  ÉLÉMENT LE PLUS FRÉQUENT  *
 ******************************/
// À titre d'exemple, on utilise une signature alternative. Remarquez que le
// nombre d'éléments dans le tableaux est fixe (N_ENTIERS).
int most_frequent(int integers[N_ENTIERS]) {
    int max = 0, maxf = 0;
    for (size_t i = 0; i < N_ENTIERS; ++i) {
        int f=1;
        for (size_t j = i+1; j < N_ENTIERS; ++j) {
            if (integers[i] == integers[j]) {
                ++f;
            }
        }
        if (f>maxf) {
            maxf=f;
            max = integers[i];
        }
    }
    return max;
}

/*********************************
*  Point d'entrée du programme  *
*********************************/
int main(int argc, char *argv[]) {
    // Ne pas se soucier de cette ligne sauf si curieux. Le cas échéant, voir le
    // fichier print_array.h
    init_printer();

    int s = sum(MY_INTEGERS, N_ENTIERS);
    printf("La somme des nombres du tableau %s est %d\n",
           array_to_str(MY_INTEGERS, N_ENTIERS), s);

    bool sorted = is_sorted(MY_INTEGERS, N_ENTIERS);
    printf("Le tableau %s %s trié\n",
            array_to_str(MY_INTEGERS, N_ENTIERS),
            sorted ? "est" : "n'est pas");
    sorted = is_sorted(MY_OTHER_INTEGERS, N_ENTIERS_PAS_TRIE);
    printf("Le tableau %s %s trié\n",
            array_to_str(MY_UNSORTED_INTEGERS, N_ENTIERS_PAS_TRIE),
            sorted ? "est" : "n'est pas");

    int m = most_frequent(MY_OTHER_INTEGERS);
    printf("Le nombre le plus fréquent du tableau %s est %d\n",
            array_to_str(MY_OTHER_INTEGERS, N_ENTIERS), m);
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

