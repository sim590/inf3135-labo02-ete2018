//----------------------------------------------------------
// Fichier: print_array.h       Auteur(s): Simon Désaulniers
// Date: 2018-01-22
//----------------------------------------------------------
// Affichage d'un tableau (pas dans le labo)
//----------------------------------------------------------

#ifndef PRINT_ARRAY_H_OJEJCWGZ
#define PRINT_ARRAY_H_OJEJCWGZ

#include <string.h>
#include <stdlib.h>

// Pourquoi 21? Voir le lien suivant:
// https://stackoverflow.com/questions/9655202/how-to-convert-integer-to-string-in-c#9655242
#define BUFLEN 21
char* int_buf;

void init_printer() { int_buf = malloc(BUFLEN*sizeof(char)); }

char* array_to_str(int* integers, int n) {
    char* array_str = malloc(sizeof(char));
    array_str[0] = '[';
    for (size_t i = 0; i < n; ++i) {
        sprintf(int_buf, "%d", integers[i]);
        array_str = strcat(array_str, int_buf);
        if (i < n-1)
            array_str = strcat(array_str, ", ");
    }
    array_str = strcat(array_str, "]");
    return array_str;
}

#endif /* end of include guard: PRINT_ARRAY_H_OJEJCWGZ */

/* vim:set et sw=4 ts=4 tw=120: */

