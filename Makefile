CC     := gcc
CFLAGS := $(if $(DEBUG),-g,) -c -Wall --std=c11

CFILES := hello.c array.c
BINARIES := hello array

all: hello array

array: array.o
	$(CC) -o $@ $<

hello: hello.o
	$(CC) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) $<

clean:
	rm -f $(CFILES:.c=.o) $(BINARIES)

# vim:set noet sts=0 sw=4 ts=4 tw=120:

